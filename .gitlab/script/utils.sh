#!/bin/bash

function log() {
  echo -e "\033[1;33m$1\033[0m"
}

function log_error() {
  echo -e "\033[1;31m$1\033[0m"
}

function log_success() {
  echo -e "\033[1;32m$1\033[0m"
}

function log_info() {
  echo -e "\033[1;35m$1\033[0m"
}

function log_with_header() {
  length=$(echo "$1" | awk '{print length}')
  delimiter=$(head -c $length </dev/zero | tr '\0' "${2:-=}")

  log_info "$delimiter"
  log_info "$1"
  log_info "$delimiter"
}

function dependabot_version() {
  echo "$(awk '/dependabot-omnibus \([0-9.]+\)/ {print $2}' Gemfile.lock | sed 's/[()]//g')"
}

function install_qemu() {
  docker pull -q "${QEMU_IMAGE}"
  docker run --rm --privileged "${QEMU_IMAGE}" --uninstall qemu-*
  docker run --rm --privileged "${QEMU_IMAGE}" --install all
}

function setup_buildx() {
  docker buildx create --use --bootstrap
}

function setup_git() {
  local ref=${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME:-$CI_COMMIT_BRANCH}

  log_with_header "Setting up git for local operations"

  log_info "Checking if GIT_PUSH_TOKEN is present"
  if [ -z "${GIT_PUSH_TOKEN:-}" ]; then
    log_error "GIT_PUSH_TOKEN is not present!"
    log_error "Please create project access token with write_repository permissions and add GIT_PUSH_TOKEN variable in your GitLab project CI/CD settings"
    exit 1
  else
    log_success "GIT_PUSH_TOKEN is present"
  fi

  log_info "Configuring git"

  log "Setting up git user"
  if [ -z "${GITLAB_USER_EMAIL}" ]; then
    log_error "GITLAB_USER_EMAIL is not present!"
    log_error "Using default email 'ci@example.com'"
    log_error "You may need to remove valid user validation under 'Repository > Push rules' settings"
    GITLAB_USER_EMAIL="ci@example.com"
  fi
  git config --global user.name "CI"
  git config --global user.email "$GITLAB_USER_EMAIL"
  log_success "Successfully set git user to 'CI <$GITLAB_USER_EMAIL>'"

  log_info "Setting origin to 'gitlab.com/${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH:-$CI_PROJECT_PATH}'"
  git remote set-url origin "https://git-push-token:${GIT_PUSH_TOKEN}@gitlab.com/${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH:-$CI_PROJECT_PATH}.git"
  log_success "Successfully set origin"

  log_info "Fetching origin and checking out branch"
  git fetch --no-tags && git checkout $ref
  log_success "Successfully configured git and set current branch to '$ref'"
}
