# ======================================================================================================================
# Global
# ======================================================================================================================
stages:
  - compile
  - build
  - static analysis
  - test
  - report
  - release
  - deploy

default:
  image: registry.gitlab.com/dependabot-gitlab/ci-images:latest
  interruptible: true

variables:
  # App tags
  CURRENT_TAG: ${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
  LATEST_TAG: ${CI_COMMIT_REF_SLUG}-latest
  # App images
  APP_IMAGE_NAME: $CI_REGISTRY_IMAGE
  BUNDLER_IMAGE_NAME: $CI_REGISTRY_IMAGE/bundler
  APP_IMAGE: ${APP_IMAGE_NAME}:${CURRENT_TAG}
  BUNDLER_IMAGE: ${BUNDLER_IMAGE_NAME}:${CURRENT_TAG}
  MOCK_IMAGE: thiht/smocker:0.18.5
  # GitLab security templates
  DEPENDENCY_SCANNING_DISABLED: "true"
  SAST_DISABLED: "true"
  CODE_QUALITY_DISABLED: "true"
  LICENSE_MANAGEMENT_DISABLED: "true"
  # Misc
  CODACY_VERSION: "13.14.0"
  BUILD_PLATFORM: linux/amd64
  PLAYWRIGHT_BROWSERS_PATH: $CI_PROJECT_DIR/.local-browsers
  # Manual web triggers
  VERSION_COMPONENT:
    description: The component of the version to increment.
    value: "none"
    options:
      - "major"
      - "minor"
      - "patch"
      - "pre"
      - "none"
  DEPLOY:
    description: Trigger a deployment.
    value: "false"
    options:
      - "true"
      - "false"

include:
  - local: .gitlab/ci/*.gitlab-ci.yml

# ======================================================================================================================
# Pre Stage
# ======================================================================================================================
dont-interrupt:
  extends:
    - .dont-interrupt
    - .rules:dont-interrupt

cache-dependencies:
  extends:
    - .cache_dependencies
    - .rules:cache

# ======================================================================================================================
# Compile Stage
# ======================================================================================================================
compile-assets:
  extends:
    - .compile_assets
    - .rules:compile-assets
  needs:
    - job: cache-dependencies
      optional: true

build-docs:
  extends:
    - .build_docs
    - .rules:build-docs
  needs:
    - job: cache-dependencies
      optional: true

# ======================================================================================================================
# Build Stage
# ======================================================================================================================
build-core-image:
  extends:
    - .build_core_image
    - .rules:build-core-image
  needs:
    - job: compile-assets
      artifacts: true

build-bundler-image:
  extends:
    - .build_bundler_image
    - .rules:build-core-image
  needs: []

build-ecosystem-images:
  extends:
    - .build_ecosystem_images
    - .rules:build-ecosystem-images
  needs: []

# ======================================================================================================================
# Static analysis stage
# ======================================================================================================================
rubocop:
  extends:
    - .rubocop
    - .rules:main
  needs:
    - job: cache-dependencies
      optional: true

reek:
  extends:
    - .reek
    - .rules:main
  needs:
    - job: cache-dependencies
      optional: true

brakeman:
  extends:
    - .brakeman
    - .rules:main
  needs: []

dependency-scan:
  extends:
    - .dependency_scan
    - .rules:dependency-scan
  needs: []

# ======================================================================================================================
# Test Stage
# ======================================================================================================================
rspec unit-test-standalone:
  extends:
    - .unit-test-standalone
    - .rules:main
  needs:
    - job: cache-dependencies
      optional: true

rspec unit-test-service:
  extends:
    - .unit-test-service
    - .rules:main
  needs:
    - job: cache-dependencies
      optional: true

rspec system-test:
  extends:
    - .system-test
    - .rules:main
  needs:
    - job: cache-dependencies
      optional: true

playwright e2e-test:
  extends:
    - .e2e-test
    - .rules:e2e-test
  needs:
    - job: build-core-image
    - job: build-bundler-image
    - job: cache-dependencies
      optional: true

migration-test:
  extends:
    - .migration-test
    - .rules:migration-test
  needs:
    - job: cache-dependencies
      optional: true

standalone-full-run-test:
  extends:
    - .standalone-test
    - .rules:standalone-test
  needs: [build-bundler-image]

# ======================================================================================================================
# Reporting
# ======================================================================================================================
publish-coverage:
  extends:
    - .coverage
    - .rules:coverage
  needs:
    - job: rspec unit-test-standalone
    - job: rspec unit-test-service
    - job: rspec system-test
    - job: cache-dependencies
      optional: true

publish-e2e-report:
  extends:
    - .e2e-report
    - .rules:e2e-test-report
  needs:
    - job: playwright e2e-test
      optional: true

# ======================================================================================================================
# Release Stage
# ======================================================================================================================
release-image:
  extends:
    - .release_image
    - .rules:release
  dependencies: []

create-gitlab-release:
  extends:
    - .gitlab_release
    - .rules:release
  needs: [release-image]

update-helm-chart:
  extends:
    - .update_chart
    - .rules:release
  needs: [release-image]

update-standalone-repo:
  extends:
    - .update_standalone
    - .rules:release
  needs: [release-image]

pages:
  extends:
    - .publish_docs
    - .rules:release-docs
  needs:
    - build-docs
    - release-image

bump-version:
  extends:
    - .bump_version
    - .rules:bump-version

# ======================================================================================================================
# Deploy Stage
# ======================================================================================================================
deploy:
  extends:
    - .deploy
    - .rules:deploy
