# frozen_string_literal: true

require_relative "util"

class StandaloneReleaseHelper
  CI_FILE = ".gitlab-ci.yml"
  TEMPLATE_FILE = "templates/template.yml"
  README = "README.md"

  PROJECT = "dependabot-gitlab/dependabot-standalone"

  def initialize(version)
    @ver = SemVer.parse(version)
    @branch = "main"
  end

  def self.call(version)
    new(version).update
  end

  def update
    logger.info("Updating dependabot-standalone image version")

    gitlab.create_commit(PROJECT,
                         branch,
                         "Update dependabot-gitlab version to #{release_app_version}\n\nchangelog: dependency",
                         update_actions)

    gitlab.create_tag(PROJECT, standalone_release_version, "main")
  end

  private

  include Util

  attr_reader :ver, :branch

  # Version number
  #
  # @return [String]
  def release_app_version
    @release_app_version ||= ver.format(VERSION_FORMAT_PATTERN)
  end

  # Semver compatible version
  #
  # @return [String]
  def standalone_release_version
    @standalone_release_version ||= ver.format(CLEAN_SEMVER_PATTERN)
  end

  # Version currently defined
  #
  # @return [String]
  def current_app_version
    @current_app_version ||= ci_files[CI_FILE].match(/DEPENDABOT_GITLAB_VERSION: (?<tag>\S+)/)[:tag]
  end

  # Current version of standalone repo
  #
  # @return [String]
  def current_standalone_version
    @current_standalone_version ||= SemVer.parse(current_app_version).format(CLEAN_SEMVER_PATTERN)
  end

  # Files to update app version numbers
  #
  # @return [Hash]
  def ci_files
    @ci_files ||= {
      CI_FILE => gitlab.file_contents(PROJECT, CI_FILE, branch),
      TEMPLATE_FILE => gitlab.file_contents(PROJECT, TEMPLATE_FILE, branch)
    }
  end

  # Standalone repo readme
  #
  # @return [String]
  def readme
    @readme ||= gitlab.file_contents(PROJECT, README, branch)
  end

  # Updated files
  #
  # @return [Array<Hash>]
  def update_actions
    ci_files.map do |path, file|
      {
        action: "update",
        file_path: path,
        content: file.gsub(current_app_version, release_app_version)
      }
    end.push({
      action: "update",
      file_path: README,
      content: readme.gsub(current_standalone_version, standalone_release_version)
    })
  end
end
