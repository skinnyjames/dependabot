# frozen_string_literal: true

describe NotifyReleaseJob, type: :job do
  subject(:job) { described_class }

  let(:dependency_name) { "rspec" }
  let(:package_ecosystem) { "bundler" }
  let(:args) { [dependency_name, package_ecosystem] }

  before do
    allow(Job::Routers::NotifyRelease).to receive(:call)
  end

  it { is_expected.to be_retryable false }

  it "queues job in low queue" do
    expect { job.perform_later(*args) }.to enqueue_sidekiq_job.on("low")
  end

  it "triggers updates" do
    job.perform_now(*args)

    expect(Job::Routers::NotifyRelease).to have_received(:call).with(dependency_name, package_ecosystem)
  end
end
