# frozen_string_literal: true

describe Mr::UpdateJob, type: :job do
  subject(:job) { described_class }

  let(:project_name) { "project" }
  let(:mr_iid) { 1 }

  before do
    allow(Job::Routers::MergeRequest::Update).to receive(:call)
  end

  it { is_expected.to be_retryable false }

  it "performs enqued job" do
    job.perform_now(project_name, mr_iid)

    expect(Job::Routers::MergeRequest::Update).to have_received(:call).with(
      project_name: project_name,
      mr_iid: mr_iid,
      ecosystem_update: false
    )
  end
end
