# frozen_string_literal: true

require_relative "../authenticated_endpoint"

api_prefix = "/api/projects"

RSpec.shared_examples "successful registration" do
  it "handles event", :aggregate_failures do
    api_post(path, { event_name: event_name, **request_args })

    expect_status(201)
    expect_json(message: "success")
    expect(Webhooks::SystemHookHandler).to have_received(:call).with(**handler_args)
  end
end

describe V1::Projects, :aggregate_failures, type: :request do
  include_context "with api helper"

  describe api_prefix do
    let(:path) { api_prefix }
    let(:project) { build(:project) }

    before do
      allow(Project).to receive(:all).and_return([project])
    end

    context "basic auth integraton", :integration do
      before do
        api_get(path)
      end

      it_behaves_like "basic auth endpoint"
    end

    context "get" do
      it "lists registered projects" do
        api_get(path)

        expect_status(200)
        expect(response.body).to eq([project.to_hash].to_json)
      end
    end

    context "create" do
      let(:project_access_token) { "test-token" }
      let(:creator_return) { project }

      before do
        allow(Dependabot::Projects::Creator).to receive(:call) { creator_return }
        allow(Cron::JobSync).to receive(:call).with(project)
      end

      context "when project is not archived" do
        it "creates project and jobs" do
          api_post(path, { project: project.name, gitlab_access_token: project_access_token })

          expect_status(201)
          expect(response.body).to eq(project.to_hash.to_json)
          expect(Cron::JobSync).to have_received(:call).with(project)
          expect(Dependabot::Projects::Creator).to have_received(:call).with(
            project.name,
            access_token: project_access_token
          )
        end
      end

      context "when project is archived" do
        let(:creator_return) { nil }

        it "returns error" do
          api_post(path, { project: project.name })
          expect_status(401)
        end
      end
    end
  end

  describe "#{api_prefix}/:id", :integration do
    let(:path) { api_prefix }
    let(:project) { create(:project) }
    let(:user) { create(:user) }

    context "basic auth integraton" do
      before do
        api_get(path)
      end

      it_behaves_like "basic auth endpoint"
    end

    context "get" do
      it "returns single project using project name" do
        api_get("#{path}/#{CGI.escape(project.name)}")

        expect_status(200)
        expect_json(project.to_hash.deep_symbolize_keys)
      end

      it "returns single project using project id" do
        api_get("#{path}/#{project.id}")

        expect_status(200)
        expect_json(project.to_hash.deep_symbolize_keys)
      end

      it "returns 404 when project is not found" do
        api_get("#{path}/not-found")

        expect_status(404)
        expect_json(error: 'Document not found for class Project with attributes {:name=>"not-found"}.')
      end
    end

    context "update" do
      let(:new_name) { Faker::Alphanumeric.alpha(number: 10) }

      it "updates project" do
        api_put("#{path}/#{project.id}", { name: new_name })

        project.reload

        expect_status(200)
        expect(response.body).to eq(project.to_hash.to_json)
        expect(project.name).to eq(new_name)
      end
    end

    context "delete" do
      it "removes registered project and jobs" do
        api_delete("#{path}/#{project.id}")

        expect_status(204)
        expect(Project.where(id: project.id).first).to be_nil
      end
    end
  end

  describe "#{api_prefix}/registration" do
    let(:with_basic_auth) { false }
    let(:path) { "#{api_prefix}/registration" }
    let(:project_name) { "project-name" }
    let(:old_project_name) { "old-project-name" }

    let(:request_args) { { path_with_namespace: project_name } }
    let(:handler_args) { { event_name: event_name, project_name: project_name, old_project_name: nil } }

    before do
      allow(Webhooks::SystemHookHandler).to receive(:call).and_return("success")
    end

    context "with project namespace filter" do
      let(:event_name) { "project_create" }
      let(:project_name) { "namespace_1/project-name" }

      before do
        allow(AppConfig).to receive(:project_registration_allow_pattern).and_return("allowed-namespace")
      end

      it "does not register project from not allowed namespace", :aggregate_failures do
        api_post(path, { event_name: event_name, **request_args })

        expect_status(202)
        expect_json(message: "Skipped, does not match allowed namespace pattern")
      end
    end

    context "with project_create event" do
      let(:event_name) { "project_create" }

      it_behaves_like "successful registration"
    end

    context "with project_destroy event" do
      let(:event_name) { "project_destroy" }

      it_behaves_like "successful registration"
    end

    context "with project_rename event" do
      let(:event_name) { "project_rename" }
      let(:request_args) { { path_with_namespace: project_name, old_path_with_namespace: old_project_name } }
      let(:handler_args) { { event_name: event_name, project_name: project_name, old_project_name: old_project_name } }

      it_behaves_like "successful registration"
    end

    context "with project_transfer event" do
      let(:event_name) { "project_transfer" }
      let(:request_args) { { path_with_namespace: project_name, old_path_with_namespace: old_project_name } }
      let(:handler_args) { { event_name: event_name, project_name: project_name, old_project_name: old_project_name } }

      it_behaves_like "successful registration"
    end

    context "with unsuccessful response" do
      let(:error) { StandardError.new("Unexpected") }
      let(:auth_token) { "auth_token" }

      before do
        allow(Webhooks::SystemHookHandler).to receive(:call).and_raise(error)
      end

      it "handles invalid request" do
        api_post(path, { "funky" => "object" })

        expect_status(400)
        expect_json(error: "event_name is missing")
      end

      it "handles unsupported event" do
        api_post(path, { event_name: "not_supported" })

        expect_status(202)
        expect_json(message: "Skipped, event not supported")
      end
    end
  end
end
