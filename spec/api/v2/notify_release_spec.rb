# frozen_string_literal: true

require_relative "../authenticated_endpoint"

api_path = "/api/v2/notify_release"

describe V2::NotifyRelease, type: :request do
  subject(:trigger_update) do
    api_post(api_path, { dependency_name: dependency_name, package_ecosystem: package_ecosystem })
  end

  include_context "with api helper"

  let(:dependency_name) { "rspec" }
  let(:package_ecosystem) { "bundler" }

  before do
    allow(NotifyReleaseJob).to receive(:perform_later)

    trigger_update
  end

  describe api_path do
    context "basic auth integraton", :integration do
      it_behaves_like "basic auth endpoint"
    end

    context "post" do
      it "triggers updates" do
        expect_status(201)
        expect_json(dependency_update_triggered: true)
        expect(NotifyReleaseJob).to have_received(:perform_later).with(dependency_name, package_ecosystem)
      end
    end
  end
end
