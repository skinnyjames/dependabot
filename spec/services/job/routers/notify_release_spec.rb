# frozen_string_literal: true

describe Job::Routers::NotifyRelease do
  let(:dependency_name) { "rspec" }
  let(:package_ecosystem) { "bundler" }
  let(:container_runner_class) { Container::Compose::Runner }

  before do
    allow(container_runner_class).to receive(:call)
  end

  around do |example|
    with_env("SETTINGS__DEPLOY_MODE" => "compose") { example.run }
  end

  it "triggers updates" do
    described_class.call(dependency_name, package_ecosystem)

    expect(container_runner_class).to have_received(:call).with(
      package_ecosystem: package_ecosystem,
      task_name: "notify_release",
      task_args: [dependency_name, package_ecosystem]
    )
  end
end
