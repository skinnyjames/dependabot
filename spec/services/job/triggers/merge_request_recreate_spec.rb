# frozen_string_literal: true

describe Job::Triggers::MergeRequestRecreate do
  let(:project_name) { "project_name" }
  let(:mr_iid) { "mr_iid" }
  let(:discussion_id) { 123 }

  before do
    allow(Dependabot::MergeRequest::RecreateService).to receive(:call)
  end

  it "calls recreate service" do
    described_class.call(project_name, mr_iid, discussion_id)

    expect(Dependabot::MergeRequest::RecreateService).to have_received(:call).with(
      project_name: project_name,
      mr_iid: mr_iid,
      discussion_id: discussion_id
    )
  end
end
