# frozen_string_literal: true

require "tempfile"

describe Dependabot::Options::All do
  subject(:parsed_config) { described_class.new(config_yml, project_name).transform }

  include_context "with dependabot helper"

  shared_examples "options parser" do |type|
    around do |example|
      with_env("RAISE_ON_SCHEMA_ERROR" => type == :schema_error ? "true" : nil) { example.run }
    end

    context "with valid configuration" do
      let(:config_yml) { File.read("spec/fixture/gitlab/responses/dependabot.yml") }

      it "returns parsed configuration" do
        expect(parsed_config).to eq({ updates: updates_config, registries: registries })
      end
    end

    context "with explicit auto-merge rules" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: daily
              auto-merge:
                squash: true
                allow:
                  - dependency-name: test
                ignore:
                  - dependency-name: test-2
        YAML
      end

      it "sets auto-merge rules" do
        expect(parsed_config[:updates].first[:auto_merge]).to eq({
          squash: true,
          merge_train: false,
          allow: [{ dependency_name: "test" }],
          ignore: [{ dependency_name: "test-2" }]
        })
      end
    end

    context "with base config template" do
      let(:config_yml) do
        <<~YAML
          version: 2
          registries:
            npm:
              type: npm-registry
              url: https://npm.pkg.github.com
              token: test_token
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: daily
        YAML
      end

      let(:base_config) do
        Tempfile.new("template.yml").tap do |f|
          f.write(base_yml)
          f.close
        end
      end

      before do
        allow(DependabotConfig).to receive(:config_base_filename).and_return(base_config.path)
      end

      context "with valid base config" do
        let(:base_yml) do
          <<~YML
            updates:
              milestone: "4"
              rebase-strategy:
                on-approval: true
          YML
        end

        it "merges base configuration" do
          expect(parsed_config[:registries]).to eq({
            "npm" => { "registry" => "npm.pkg.github.com", "token" => "test_token", "type" => "npm_registry" }
          })
          expect(parsed_config[:updates].first).to include({
            milestone: "4",
            rebase_strategy: { strategy: "auto", on_approval: true, with_assignee: nil }
          })
        end
      end

      context "with invalid updates base config" do
        let(:base_yml) do
          <<~YML
            updates:
              - milestone: "4"
                rebase-strategy:
                  on-approval: true
          YML
        end

        it "raises invalid updates key error" do
          expect { parsed_config }.to raise_error(
            ContractSchemaError,
            "`updates` key in base configuration `#{base_config.path}` must be a map!"
          )
        end
      end
    end

    context "with registries" do
      let(:config_yml) do
        <<~YAML
          version: 2
          registries:
            dockerhub:
              type: docker-registry
              url: https://registry.hub.docker.com
              username: octocat
              password: password
            npm:
              type: npm-registry
              url: https://npm.pkg.github.com
              token: test_token
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: daily
                hours: 9-17
              registries:
                - npm
        YAML
      end

      it "sets reject_external_code: true" do
        expect(parsed_config[:updates].first[:reject_external_code]).to eq(true)
      end
    end

    context "without registries" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
        YAML
      end

      it "sets reject_external_code: false by default" do
        expect(parsed_config[:updates].first[:reject_external_code]).to eq(false)
      end
    end

    context "with disabled vulnerabilities alerts" do
      let(:config_yml) do
        <<~YAML
          version: 2
          vulnerability-alerts:
            enabled: false
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
        YAML
      end

      it "sets vulnerability alerts to disabled" do
        expect(parsed_config[:updates].first.dig(:vulnerability_alerts, :enabled)).to eq(false)
      end
    end

    context "with valid config and rebase on approvals" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
              rebase-strategy:
                on-approval: true
        YAML
      end

      it "sets 'auto' strategy by default" do
        expect(parsed_config[:updates].first[:rebase_strategy]).to eq({
          strategy: "auto",
          on_approval: true,
          with_assignee: nil
        })
      end
    end

    context "with missing or incorrect types in config" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              vendor: "true"
              schedule:
                interval: daily
                time: "19:00"
              milestone: 4
              ignore:
                - versions: ["3.x", "4.x"]
              commit-message:
                prefix: "dep"
                trailers:
                  - changelog: "dep"
        YAML
      end

      if type == :contract_error
        let(:validation_error) { ContractSchemaError }
        let(:invalid_config_error) do
          <<~ERR.strip
            key 'updates.0.directory' is missing
            key 'updates.0.ignore.0.dependency-name' is missing
            key 'updates.0.milestone' must be a string
            key 'updates.0.vendor' must be boolean
          ERR
        end
      else
        let(:validation_error) { RuntimeError }
        let(:invalid_config_error) do
          <<~ERR.strip
            Validation for dependabot.yml failed:
            The property '#/updates/0' did not contain a required property of 'directory'
            The property '#/updates/0/ignore/0' did not contain a required property of 'dependency-name'
            The property '#/updates/0/milestone' of type integer did not match the following type: string
            The property '#/updates/0/vendor' of type string did not match the following type: boolean
          ERR
        end
      end

      it "throws invalid configuration error" do
        expect { parsed_config }.to raise_error(
          validation_error, invalid_config_error
        )
      end
    end

    context "with incorrect hours range format" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
                hours: "25"
        YAML
      end

      it "throws invalid format error" do
        expect { parsed_config }.to raise_error(
          ContractSchemaError,
          "key 'schedule.hours.0' has invalid format, must match pattern '^\\d{1,2}-\\d{1,2}$'"
        )
      end
    end

    context "with incorrect hours range definition" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
                hours: "23-0"
        YAML
      end

      it "throws invalid format error" do
        expect { parsed_config }.to raise_error(
          ContractSchemaError,
          "key 'schedule.hours.0' has invalid format, first number in range must be smaller or equal than second"
        )
      end
    end

    context "with invalid rebase-strategy configuration" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
              rebase-strategy:
                approval: true
        YAML
      end

      it "sets reject_external_code: false by default" do
        expect { parsed_config }.to raise_error(
          ContractSchemaError,
          "key 'rebase-strategy.approval' is not allowed"
        )
      end
    end

    context "with invalid groups configuration" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
              groups:
                test:
                  invalid-key: ["test"]
        YAML
      end

      it "throws contract schema error" do
        expect { parsed_config }.to raise_error(
          ContractSchemaError,
          "group 'test' has schema errors: key 'invalid-key' is not allowed, key 'patterns' is missing"
        )
      end
    end
  end

  it_behaves_like "options parser", :contract_error
  it_behaves_like "options parser", :schema_error
end
