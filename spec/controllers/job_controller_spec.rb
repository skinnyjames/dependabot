# frozen_string_literal: true

describe JobController, :integration, :anonymous_access, type: :request do
  include_context "with api helper"

  let(:project) { create(:project) }
  let(:update_job) { project.update_jobs.first }
  let(:job_name) { update_job.name }

  context "with execute action" do
    before do
      allow(UpdateRunnerJob).to receive(:perform_later)
    end

    it "enqueues dependency update job", :aggregate_failures do
      put("/jobs/#{update_job.id}/execute")

      expect_status(200)
      expect(UpdateRunnerJob).to have_received(:perform_later).with(
        project_name: project.name,
        package_ecosystem: update_job.package_ecosystem,
        directory: update_job.directory
      )
    end
  end

  context "with toggle action" do
    let(:cron_job) do
      Sidekiq::Cron::Job.new(
        name: job_name,
        cron: "0 0 * * *",
        class: UpdateRunnerJob.to_s,
        active_job: true,
        description: "Test",
        status: initial_status,
        args: {
          "project_name" => project.name,
          "package_ecosystem" => update_job.package_ecosystem,
          "directory" => update_job.directory
        }
      ).tap(&:save)
    end

    before do
      cron_job

      put("/jobs/#{update_job.id}/toggle", params: { enabled: initial_status == "disabled" })
    end

    context "with enabled param set to true" do
      let(:initial_status) { "disabled" }

      it "disables update job", :aggregate_failures do
        expect_status(200)
        expect(Sidekiq::Cron::Job.find(job_name).status).to eq("enabled")
        expect(update_job.reload.enabled).to eq(true)
      end
    end

    context "with enabled param set to false" do
      let(:initial_status) { "enabled" }

      it "disables update job", :aggregate_failures do
        expect_status(200)
        expect(Sidekiq::Cron::Job.find(job_name).status).to eq("disabled")
        expect(update_job.reload.enabled).to eq(false)
      end
    end
  end
end
