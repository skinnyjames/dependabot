import { v2 as compose } from "docker-compose";
import { env } from "process";
import { readFileSync } from "fs";
import yaml from "js-yaml";
import { user } from "./user";

const composeProjectName = env.COMPOSE_PROJECT_NAME || "dependabot";
const mockImage = env.MOCK_IMAGE || "thiht/smocker:0.18.5";
const baseImage = env.APP_IMAGE || env.BASE_IMAGE;
const updaterImagePattern = env.UPDATER_IMAGE_PATTERN;

const composeYml = (() => {
  const yml = yaml.load(readFileSync("docker-compose.yml") as any) as any;
  yml["services"]["gitlab"] = {
    image: mockImage,
    ports: ["8081:8081"]
  };

  return yaml.dump(yml, {
    styles: {
      "!!null": "empty"
    }
  });
})();

const composeEnv = {
  BASE_IMAGE: baseImage,
  UPDATER_IMAGE_PATTERN: updaterImagePattern,
  REDIS_EXTRA_FLAGS: "--protected-mode no",
  SETTINGS__GITLAB_URL: "http://gitlab:8080",
  SETTINGS__DEPENDABOT_URL: "http://dependabot-gitlab.com",
  SETTINGS__GITLAB_ACCESS_TOKEN: "e2e-test",
  SETTINGS__GITHUB_ACCESS_TOKEN: env.GITHUB_ACCESS_TOKEN_TEST,
  SETTINGS__LOG_COLOR: "true",
  SETTINGS__ANONYMOUS_ACCESS: "false"
};

const composeOpts: compose.IDockerComposeOptions = {
  env: { ...env, ...composeEnv },
  composeOptions: ["--project-name", composeProjectName],
  log: false
};

export const setupE2eEnvironment = async () => {
  const options = { ...composeOpts, configAsString: composeYml };
  console.log("*** Setting up E2E environment ***");
  console.log("Pulling images...");
  await compose.pullAll({ ...options, commandOptions: ["--include-deps", "-q"] });
  console.log("  successfully pulled images");

  console.log("Starting services...");
  await compose.upAll({ ...options, commandOptions: ["--wait"] });
  console.log("  successfully started test environment");

  try {
    console.log(`Creating test user '${user.name}'...`);
    await compose.exec("web", `bundle exec rake dependabot:create_user[${user.name},${user.password}]`, options);
    console.log("  successfully created test user");
  } catch (error) {
    if (error.err.includes(`Username '${user.name}' has already been taken`)) {
      console.log("  user already exists");
      return;
    }
    console.log("  failed to create test user");
    throw error;
  }
};

export const teardownE2eEnvironment = async () => {
  console.log("*** Tearing down E2E environment ***");
  console.log("Stopping services...");
  await compose.stop({ ...composeOpts, configAsString: composeYml });
  console.log("  successfully stopped test environment");
};
