import { test } from "@playwright/test";
import { SignInPage } from "@pages/signIn";
import { ProjectsPage } from "@pages/projects";
import { Smocker } from "@support/mocks/smocker";
import { mocks } from "@support/mocks/mocks";
import { user } from "@support/user";

import randomstring from "randomstring";

let smocker: Smocker;
let projectName: string;

test.beforeEach(async ({ page }) => {
  projectName = randomstring.generate({ length: 10, charset: "alphabetic" });
  smocker = await new Smocker().init();

  await smocker.reset();

  const signInPage = new SignInPage(page);
  await signInPage.visit();
  await signInPage.signIn(user.name, user.password);
});

test.afterEach(async () => {
  await smocker.verify();
  await smocker.dispose();
});

test("renders main page", async ({ page }) => {
  const projectsPage = new ProjectsPage(page);
  await projectsPage.expectPageToBeVisible();
});

test("adds new project without specific token", async ({ page }) => {
  const projectsPage = new ProjectsPage(page);

  await smocker.add(mocks.registerProject(projectName));
  await projectsPage.addProject(projectName);

  await projectsPage.searchForProject(projectName);
  await projectsPage.expectProjectToBeVisible(projectName);
});

test("adds new project with specific token", async ({ page }) => {
  const projectsPage = new ProjectsPage(page);
  const token = "token";

  await smocker.add(mocks.registerProject(projectName));
  await projectsPage.addProject(projectName, token);

  await projectsPage.searchForProject(projectName);
  await projectsPage.expectProjectToBeVisible(projectName);
});
