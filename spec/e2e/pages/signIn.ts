import { Locator, Page, expect } from "@playwright/test";

export class SignInPage {
  private readonly page: Page;

  private readonly usernameInput: Locator;
  private readonly passwordInput: Locator;
  private readonly signInButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.usernameInput = page.locator("id=username");
    this.passwordInput = page.locator("id=password");
    this.signInButton = page.locator("id=sign_in");
  }

  async visit() {
    await this.page.goto("/sign_in");
  }

  async expectPageToBeVisible() {
    await expect(this.signInButton).toBeVisible();
    await expect(this.signInButton).toBeVisible();
    await expect(this.signInButton).toBeVisible();
  }

  async signIn(username: string, password: string) {
    await this.expectPageToBeVisible();

    await this.usernameInput.fill(username);
    await this.passwordInput.fill(password);
    await this.signInButton.click();

    await this.page.waitForURL("/projects");
  }
}
