# frozen_string_literal: true

class DependabotConfig < ApplicationConfig
  env_prefix :settings_

  attr_config :config_branch,
              :config_base_filename,
              :config_local_filename,
              config_filename: ".gitlab/dependabot.yml",
              dry_run: false
end
