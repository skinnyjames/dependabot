import { writeFileSync } from "fs";
import { env } from "process";

const ecosystems = [
  "bundler",
  "npm",
  "gomod",
  "pip",
  "docker",
  "composer",
  "pub",
  "cargo",
  "nuget",
  "maven",
  "gradle",
  "mix",
  "terraform",
  "elm",
  "gitsubmodule"
];
const tag = (env["CI_COMMIT_TAG"] || "latest").replace(/^v/, "");

const coreImageDocsMd = () => {
  let doc = "";

  doc += `- [Dockerhub](https://hub.docker.com/r/andrcuns/dependabot-gitlab/tags) - \`docker.io/andrcuns/dependabot-gitlab:${tag}\`\n`;
  doc += `- [GHCR](https://github.com/dependabot-gitlab/images/pkgs/container/dependabot-gitlab) - \`ghcr.io/dependabot-gitlab/dependabot-gitlab:${tag}\``

  return doc;
}

const updaterImageDocsMd = () => {
  let doc = "";

  ecosystems.forEach((ecosystem) => {
    doc += `### ${ecosystem}\n\n`;
    doc += `- [Dockerhub](https://hub.docker.com/r/andrcuns/dependabot-gitlab-${ecosystem}/tags) - \`docker.io/andrcuns/dependabot-gitlab-${ecosystem}:${tag}\`\n`;
    doc += `- [GHCR](https://github.com/dependabot-gitlab/images/pkgs/container/dependabot-gitlab-${ecosystem}) - \`ghcr.io/dependabot-gitlab/dependabot-gitlab-${ecosystem}:${tag}\`\n\n`;
  });

  return doc.trimEnd();
};

export function imageDocs() {
  writeFileSync("docs/guide/core-images.md", coreImageDocsMd());
  writeFileSync("docs/guide/updater-images.md", updaterImageDocsMd());

  return {
    link: "/guide/index.html"
  };
}
