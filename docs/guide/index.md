# What is dependabot-gitlab?

It is an app for automatically managing dependency updates.

`dependabot-gitlab` uses [dependabot-core](https://github.com/dependabot/dependabot-core) for dependency update logic and adds additional functionality to integrate these updates with GitLab.

::: warning
dependabot-gitlab project is not affiliated with, funded by, or developed by the Dependabot team, GitHub or GitLab
:::

::: warning
dependabot-gitlab is still considered in alpha status. It is already suitable for out-of-the-box dependency updates, but bugs and breaking changes are still expected between releases
:::

## Distribution

::: info
When running application in service mode, internally application will start `updater` containers dynamically and cache images. Because of that, using `latest` tag is not recommended to avoid main application image becoming out of sync with updater images. It is recommended to always use specific version tag instead.
:::

Application is packaged and release as a docker image. Application consists of 2 image types.

## Core

::: warning
Not applicable for standalone mode
:::

Core image contains code for running `web` and `worker` containers. This image does not contain any package managers or language runtimes.

Images are available from the following registries:

<!--@include: ../guide/core-images.md-->

## Updater

Updater image contains all package managers and language runtimes supported by dependabot-core. This image is used to run `updater` containers. These images are also used when running application in standalone mode.

Images are available from the following registries.

<!--@include: ../guide/updater-images.md-->
