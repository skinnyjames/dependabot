# UI

UI is served at the index page of application, like `http://localhost:3000/`.

Main projects page is served at `http://localhost:3000/projects` and following functions are supported:

- Table view of all projects and update jobs
- Adding new project
- Removing project
- Syncing project configuration
- Triggering dependency update jobs
- Enabling/disabling update jobs
