# frozen_string_literal: true

require "active_record/type"

class JobController < ApplicationController
  before_action :authenticate_user!

  def execute
    project = Project.find(job.project_id)

    UpdateRunnerJob.perform_later(
      project_name: project.name,
      package_ecosystem: job.package_ecosystem,
      directory: job.directory
    )

    render_alert("Dependency update job was successfully triggered!")
  rescue StandardError => e
    capture_error(e)
    render_alert("Failed to trigger dependency update job: #{e.message}", "danger")
  end

  # :reek:TooManyStatements
  #
  def toggle
    job_name = job.name
    cron_job = Sidekiq::Cron::Job.find(job_name)
    enabled = ActiveRecord::Type::Boolean.new.cast(params.require(:enabled))

    success = enabled ? cron_job.enable! : cron_job.disable!
    return render_alert("Failed to change cron job state", "danger") unless success

    job.update!(enabled: enabled)
    log(:info, "Set cron job '#{job_name}' state: enabled=#{enabled}")
    render_alert("Successfully #{enabled ? 'enabled' : 'disabled'} dependency update job!")
  rescue StandardError => e
    capture_error(e)
    render_alert("Failed to change dependency update state: #{e.message}", "danger")
  end

  private

  def job
    @job ||= Update::Job.find(params.require(:id))
  end

  def render_alert(message, alert_type = "success")
    render partial: "shared/alert", locals: {
      message: message,
      alert_type: alert_type,
      extra_classes: ["mx-5"]
    }
  end
end
