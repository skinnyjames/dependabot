# frozen_string_literal: true

module Update
  # Dependency update job
  #
  # @!attribute name
  #   @return [String]
  # @!attribute package_ecosystem
  #   @return [String]
  # @!attribute directory
  #   @return [String]
  # @!attribute cron
  #   @return [String]
  # @!attribute project
  #   @return [Project]
  # @!attribute runs
  #   @return [Array<Update::Run>]
  class Job
    include Mongoid::Document

    field :name, type: String
    field :package_ecosystem, type: String
    field :directory, type: String
    field :cron, type: String
    field :enabled, type: Boolean, default: true

    belongs_to :project

    has_many :runs, class_name: "Update::Run", dependent: :destroy

    validates! :name, presence: true, uniqueness: { message: "'%<value>s' has already been taken" }

    index({ name: 1 }, { unique: true })

    # Last run of the job
    #
    # @return [Run]
    def last_run
      @last_run ||= runs.last
    end

    # Last run of the job
    #
    # @return [DateTime]
    def last_executed
      last_run&.created_at
    end

    # Last finished run of the job
    #
    # @return [DateTime]
    def last_finished
      last_run&.finished_at
    end

    # Truncated failures of last run
    #
    # @return [String]
    def last_failures
      last_run
        &.failures
        &.map { |failure| "- #{failure.message.truncate(100)}" }
        &.join("\n")
    end

    class Entity < Grape::Entity
      format_with(:utc) { |dt| dt&.utc }

      expose :id, documentation: { type: String, desc: "Job id" } do |job, _options|
        job.id.to_s
      end
      expose :project_id, documentation: { type: Integer, desc: "Project ID" } do |job, _options|
        job.project.id
      end
      expose :project_name, documentation: { type: String, desc: "Project name" } do |job, _options|
        job.project.name
      end
      expose :name, documentation: { type: String, desc: "Job name" }
      expose :enabled, documentation: { type: "Boolean", desc: "Job enabled" }
      expose :package_ecosystem, documentation: { type: String, desc: "Package ecosystem" }
      expose :directory, documentation: { type: String, desc: "Directory" }
      expose :cron, documentation: { type: String, desc: "Job cron" }
      expose :last_executed, format_with: :utc, documentation: { type: DateTime, desc: "Last executed" }
      expose :last_finished, format_with: :utc, documentation: { type: DateTime, desc: "Last finished" }
      expose :last_failures, documentation: { type: String, desc: "List of last run truncated failure messages" }

      # Example response hash
      #
      # @return [Hash]
      def self.example_response
        {
          id: "5d5b5c5d5e5f5a5b5c5d5e5f",
          name: "project_name:package_ecosystem:directory",
          package_ecosystem: "npm",
          directory: "/",
          enabled: true,
          cron: "5 0 * * 6 UTC",
          last_executed: Time.zone.now,
          last_finished: Time.zone.now,
          last_failures: "- Failure message",
          project_id: 1
        }
      end
    end
  end
end
