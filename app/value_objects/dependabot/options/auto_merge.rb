# frozen_string_literal: true

module Dependabot
  module Options
    # Auto merge related options
    #
    class AutoMerge < OptionsBase
      include OptionsHelper

      # Transform auto merge options
      #
      # @return [Hash]
      def transform
        auto_merge = opts[:"auto-merge"]
        return {} unless auto_merge
        return { auto_merge: { squash: false } } if auto_merge == true

        validate_config_options(AutoMergeConfigContract, { "auto-merge": auto_merge })

        {
          auto_merge: {
            squash: auto_merge[:squash].present?,
            delay: auto_merge[:delay],
            merge_train: auto_merge[:"merge-train"].present?,
            allow: transform_rule_options(auto_merge[:allow]),
            ignore: transform_rule_options(auto_merge[:ignore])
          }.compact
        }
      end
    end
  end
end
