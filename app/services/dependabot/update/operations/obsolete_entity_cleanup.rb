# frozen_string_literal: true

module Dependabot
  module Update
    module Operations
      # Obsolete vulnerability issue and merge request cleanup
      #
      class ObsoleteEntityCleanup < ApplicationService
        using Rainbow

        def initialize(project:, updated_dependency:, directory:)
          @project = project
          @updated_dependency = updated_dependency
          @directory = directory
        end

        def call
          return if DependabotConfig.dry_run?
          return if AppConfig.standalone?

          close_obsolete_vulnerability_issues
          close_obsolete_mrs
        end

        private

        delegate :name, to: :project, prefix: true

        # @return [Project]
        attr_reader :project
        # @return [Dependabot::UpdatedDependency]
        attr_reader :updated_dependency
        # @return [String]
        attr_reader :directory

        # :reek:TooManyStatements

        # Close obsolete vulnerability issues
        #
        # @return [void]
        def close_obsolete_vulnerability_issues
          dep_name = updated_dependency.name
          log(:debug, "Checking for obsolete vulnerability issues for #{dep_name}")
          obsolete_issues = project.open_vulnerability_issues(
            package_ecosystem: package_ecosystem,
            directory: directory,
            package: dep_name
          ).reject { |issue| issue.vulnerability.vulnerable?(updated_dependency.version) }
          return log(:debug, "No obsolete issues found") if obsolete_issues.empty?

          log(:info, "Closing obsolete vulnerability issues for #{dep_name.bright}")
          obsolete_issues.each do |issue|
            Gitlab::Vulnerabilities::IssueCloser.call(issue)
            log(:info, "  closed obsolete issue !#{issue.iid}")
          rescue Gitlab::Error::ResponseError => e
            log_error(e, message_prefix: "  failed to close obsolete issue !#{issue.iid}")
          end
        end

        # Close obsolete merge requests
        #
        # @return [void]
        def close_obsolete_mrs
          dep_name = updated_dependency.name
          log(:debug, "Checking for obsolete merge requests for #{dep_name}")
          obsolete_mrs = project.open_dependency_merge_requests(dep_name, directory)
          return log(:debug, "No obsolete merge requests found") if obsolete_mrs.empty?

          log(:info, "Closing obsolete merge requests for #{dep_name.bright}")
          obsolete_mrs.each { |mr| close_mr(mr) }
        end

        # Close merge request
        #
        # @param [MergeRequest] merge_request
        # @return [void]
        def close_mr(merge_request)
          iid = merge_request.iid
          return unless mr_state_in_sync?(merge_request)

          Gitlab::BranchRemover.call(project_name, merge_request.branch)
          merge_request.close

          Gitlab::MergeRequest::Commenter.call(
            project_name,
            iid,
            "This merge request has been closed because the dependency version is up to date."
          )
          log(:info, "  closed obsolete merge request !#{iid}")
        rescue Gitlab::Error::Error => e
          log_error(e, message_prefix: "  failed to close obsolete merge request: !#{iid}")
        end

        # Check if merge request state is in sync with GitLab
        #
        # @param [MergeRequest] merge_request
        # @return [Boolean]
        def mr_state_in_sync?(merge_request)
          iid = merge_request.iid
          mr = gitlab.merge_request(project_name, iid)

          if mr.state != "opened"
            # This should not happen if webhooks are set up
            log(AppConfig.integrated? ? :warn : :debug, "  merge request !#{iid} state is not in sync with GitLab")
            merge_request.close
            return false
          end

          true
        end

        # Dependency package ecosystem
        #
        # @return [String]
        def package_ecosystem
          Ecosystem::PACKAGE_MANAGER_MAPPING.fetch(updated_dependency.package_manager,
                                                   updated_dependency.package_manager)
        end
      end
    end
  end
end
