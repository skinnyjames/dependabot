# frozen_string_literal: true

module Dependabot
  module Update
    module ServiceMode
      module Runner
        include ServiceHelpersConcern
        using Rainbow

        # Initialize gitlab client and sync config and run update
        #
        # @return [void]
        def call
          init_gitlab(project)
          sync_config

          super
        end

        private

        delegate :configuration, to: :project, prefix: :project

        # Fetch up to date config if app is not integrated
        #
        # @return [void]
        def sync_config
          return if AppConfig.integrated?

          project.configuration = fetch_config
        end

        # Persisted project
        #
        # @return [Project]
        def project
          @project ||= Project.find_by(name: project_name)
        end

        # No-op method to skip vulnerability fetching in service mode
        #
        # @return [void]
        def fetch_vulnerabilities; end
      end
    end
  end
end
