# frozen_string_literal: true

module Dependabot
  module Update
    class TooManyRequestsError < StandardError
      def message
        "GitHub API rate limit exceeded! See: https://docs.github.com/en/rest/overview/resources-in-the-rest-api#rate-limiting"
      end
    end

    class ExternalCodeExecutionError < StandardError
      def initialize(package_ecosystem, directory)
        super(<<~ERR)
          Unexpected external code execution detected.
          Option 'insecure-external-code-execution' must be set to 'allow' for entry:
            package_ecosystem - '#{package_ecosystem}'
            directory - '#{directory}'
        ERR
      end
    end

    # Main entrypoint class for updating dependencies and creating merge requests
    #
    class Runner < Base # rubocop:disable Metrics/ClassLength
      include ServiceModeConcern
      include ConfigHelper

      UPDATE_PERFORMED = "updated-or-created"
      LIMIT_REACHED = "limit-reached"
      SYSTEM_FAILURE = "system-failure"

      using Rainbow

      def initialize(project_name:, package_ecosystem:, directory:, dependency_name: nil)
        super(project_name)

        @package_ecosystem = package_ecosystem
        @directory = directory
        @dependency_name = dependency_name
      end

      # Create or update mr's for dependencies
      #
      # @return [void]
      def call
        fetch_gitlab_project
        fetch_vulnerabilities
        group_dependencies

        update
      rescue Octokit::TooManyRequests
        raise TooManyRequestsError
      rescue Dependabot::UnexpectedExternalCode
        raise ExternalCodeExecutionError.new(package_ecosystem, directory)
      ensure
        FileUtils.rm_r(repo_contents_path, force: true, secure: true) if project.configuration && repo_contents_path
      end

      private

      attr_reader :package_ecosystem, :directory, :dependency_name

      # Project
      #
      # @return [Project]
      def project
        @project ||= Project.new(name: project_name)
      end

      # Gitlab project
      #
      # @return [Gitlab::ObjectifiedHash]
      def gitlab_project
        @gitlab_project ||= gitlab.project(project_name)
      end
      alias_method :fetch_gitlab_project, :gitlab_project

      # Project configuration
      #
      # @return [Configuration]
      def project_configuration
        @project_configuration ||= fetch_config.then do |config|
          fork_id = config.forked ? gitlab_project.to_h.dig("forked_from_project", "id") : nil

          project.forked_from_id = fork_id
          project.configuration = config
        end
      end

      # Open mr limits
      #
      # @return [Number]
      def limits
        @limits ||= {
          mr: config_entry[:open_merge_requests_limit],
          security_mr: config_entry[:open_security_merge_requests_limit]
        }
      end

      # Project configuration fetched from gitlab
      #
      # @return [Configuration]
      def fetch_config
        Config::Fetcher.call(project_name)
      end

      # Fetch package ecosystem vulnerability info
      #
      # @return [void]
      def fetch_vulnerabilities
        return unless vulnerability_alerts?(config_entry)

        Github::Vulnerabilities::LocalStore.call(package_ecosystem)
      end

      # Update project dependencies
      #
      # @return [void]
      def update
        # TODO: Perform updates for grouped and ungrouped dependencies once supported in dependabot-core
        # check_group_rules
        dependencies.each_with_object({ mr: Set.new, security_mr: Set.new }) do |dependency, created_mrs|
          updated_dependency = update_dependency(dependency)

          result = create_dependency_update_mr(updated_dependency, created_mrs)
          # go to next dep if
          # * new mr was created
          # * existing mr was updated
          next if result == UPDATE_PERFORMED

          Operations::VulnerabilityIssueCreation.call(project, updated_dependency, config_entry)
          # skip obsolete entity cleanup if
          # * limit was reached, because it's possible that open mr exists if overall limit got reduced
          # * uknown error occured, which could lead to closing open mr
          #
          # in all other cases result should be nil which indicates there is nothing to update
          # and obsolete entities should be cleaned up
          next if result

          Operations::ObsoleteEntityCleanup.call(
            project: project,
            updated_dependency: updated_dependency,
            directory: directory
          )
        end
      end

      # Check group rules
      # Placeholder for future implementation
      #
      # @return [void]
      def check_group_rules
        return unless config_entry[:groups] && dependency_groups&.none? { |group| !group.dependencies.empty? }

        log(:warn, "None of the dependencies matched the rules defined in the groups configurations!")
      end

      # Create or update dependency update merge request
      # Return iid of mr if created or updated
      #
      # @param [Dependabot::UpdatedDependency] updated_dependency
      # @param [Hash] mrs
      # @return [<String, nil>]
      def create_dependency_update_mr(updated_dependency, mrs)
        type = updated_dependency.vulnerable? ? :security_mr : :mr
        return LIMIT_REACHED unless create_mr?(mrs, type)
        return unless updated_dependency.updates?

        iid = MergeRequest::CreateService.call(
          project: project,
          fetcher: fetcher,
          config_entry: config_entry,
          updated_dependency: updated_dependency,
          credentials: credentials
        )&.iid

        if iid
          mrs[type] << iid
          UPDATE_PERFORMED
        end
      rescue StandardError => e
        capture_error(e)
        SYSTEM_FAILURE
      end

      # Check if mr should be created based on limits settings
      #
      # @param [Hash] mrs
      # @param [Symbol] type
      # @return [Boolean]
      def create_mr?(mrs, type)
        limit = limits[type]

        return true if limit.negative?
        return true if !limit.zero? && (mrs[type].length < limit)

        dep_type = type == :mr ? "dependency" : "vulnerable dependency"
        reason = if limits[type].zero?
                   "due to disabled mr creation setting!"
                 else
                   "due to max open mr limit reached, limit: '#{limit}'!"
                 end

        log(:info, "  skipping update of #{dep_type} #{reason}")
        false
      end
    end
  end
end
