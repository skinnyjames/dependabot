# frozen_string_literal: true

module Container
  module Kubernetes
    # Thin wrapper around Kubeclient::Client
    #
    class Client
      def initialize
        @client = Kubeclient::Client.new(
          "https://kubernetes.default.svc",
          "v1",
          auth_options: auth_options,
          ssl_options: ssl_options
        )
      end

      def method_missing(name, *args, &block)
        client.public_send(name, *args, &block)
      end

      # :reek:ManualDispatch
      def respond_to_missing?(name, _include_private = false)
        client.respond_to?(name)
      end

      private

      attr_reader :client

      def auth_options
        {
          bearer_token_file: "/var/run/secrets/kubernetes.io/serviceaccount/token"
        }
      end

      def ssl_options
        "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt".then do |cert|
          return {} unless File.exist?(cert)

          { ca_file: cert }
        end
      end
    end
  end
end
