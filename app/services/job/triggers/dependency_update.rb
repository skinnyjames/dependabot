# frozen_string_literal: true

module Job
  module Triggers
    # Dependency update trigger class called in updater container
    #
    class DependencyUpdate < ApplicationService
      include ServiceModeConcern

      def initialize(project_name, package_ecosystem, directory)
        @project_name = project_name
        @package_ecosystem = package_ecosystem
        @directory = directory
      end

      def call
        context = job_details(
          job: "dep-update",
          project_name: project_name,
          ecosystem: package_ecosystem,
          directory: directory
        )

        run_within_context(context, dependency_updates: true) do
          Dependabot::Update::Runner.call(
            project_name: project_name,
            package_ecosystem: package_ecosystem,
            directory: directory
          )
        end

        UpdateFailures.fetch
      end

      private

      attr_reader :project_name, :package_ecosystem, :directory
    end
  end
end
