# frozen_string_literal: true

module Gitlab
  class LabelCreator < ApplicationService
    def initialize(project_name:, name:, color:)
      @project_name = project_name
      @name = name
      @color = color
    end

    def call
      log(:debug, "Creating label #{name}")
      return log(:debug, "Skipping creation of label '#{name}', already exists!") if exists?

      create_label.name
    end

    private

    attr_reader :project_name, :name, :color

    # Check if label exists
    #
    # @return [Boolean]
    def exists?
      cached_labels.include?(name) || project_labels.include?(name)
    end

    # Create project label
    #
    # @return [String]
    def create_label
      gitlab.create_label(project_name, name, color).tap do
        Rails.cache.write("#{project_name}-labels", [*cached_labels, name])
      end
    end

    # Project labels
    #
    # @return [Array]
    def project_labels
      gitlab.labels(project_name, search: name).auto_paginate.map(&:name).tap do |labels|
        next if labels.empty?

        Rails.cache.write("#{project_name}-labels", [*cached_labels, *labels])
      end
    end

    # Cached labels known to exist
    # Label creation operation is performed on mr and vulnerability issue creation
    # by caching the labels we can avoid making unnecessary api calls
    #
    # @return [Array]
    def cached_labels
      Rails.cache.fetch("#{project_name}-labels", expires_in: 24.hours) do
        []
      end
    end
  end
end
