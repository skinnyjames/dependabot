# frozen_string_literal: true

module Mr
  # Update single merge request based on received webhook event
  #
  class UpdateJob < ApplicationJob
    queue_as :high

    sidekiq_options retry: false

    # Perform merge request rebase
    #
    # @param [String] project_name
    # @param [Number] mr_iid
    # @return [void]
    def perform(project_name, mr_iid, ecosystem_update: false)
      run_within_context({ job: "mr-update", project: project_name, mr: "!#{mr_iid}" }) do
        Job::Routers::MergeRequest::Update.call(
          project_name: project_name,
          mr_iid: mr_iid,
          ecosystem_update: ecosystem_update
        )
      end
    end
  end
end
