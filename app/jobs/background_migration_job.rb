# frozen_string_literal: true

class BackgroundMigrationJob < ApplicationJob
  queue_as :low

  sidekiq_options retry: false

  # Run background migration
  #
  # @return [void]
  def perform
    run_within_context({ job: "background-migrations" }) do
      log(:info, "Running background migrations")
      return log(:info, "No pending migrations") unless migrations_pending?

      migrator.migrate
    end
  end

  private

  # Instance of migrator
  #
  # @return [Mongoid::Migrator]
  def migrator
    @migrator ||= Mongoid::Migrator.new(:up, Rails.root.join("db/migrate/background"), background_migration: true)
  end

  # Check if migrations are pending
  #
  # @return [Boolean]
  def migrations_pending?
    pending_migrations = migrator.migrations.size - migrator.migrated.size

    pending_migrations.positive?
  end
end
