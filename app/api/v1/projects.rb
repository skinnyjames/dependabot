# frozen_string_literal: true

module V1
  # @deprecated
  #
  # This API version is deprecated and will be removed in the next major release
  class Projects < Grape::API
    helpers do
      include ApplicationHelper

      # Find project
      #
      # @return [Project]
      def project
        params[:id].then do |id|
          args = id.match?(/\d+/) ? { id: id } : { name: id }
          Project.find_by(**args)
        end
      end

      # Check if namespace is allowed for project registration
      #
      # @return [Boolean]
      def allowed_namespace?
        return true unless AppConfig.project_registration_allow_pattern

        params[:path_with_namespace].match?(Regexp.new(AppConfig.project_registration_allow_pattern))
      end
    end

    namespace :projects do
      include AuthenticationSetup

      route_param :id, type: String, desc: "Project id or full path" do
        desc "Return single project"
        get do
          project.to_hash
        end

        # TODO: Implement proper configuration update in v2
        desc "Update project attributes"
        params do
          optional :name, type: String, desc: "Project name"
          optional :forked_from_id, type: Integer, desc: "Forked from project id"
          optional :forked_from_name, type: String, desc: "Forked from project name"
          optional :webhook_id, type: Integer, desc: "Webhook id"
          optional :web_url, type: String, desc: "Web url"
        end
        put do
          project.update_attributes!(**params.slice(
            :name,
            :forked_from_id,
            :forked_from_name,
            :webhook_id,
            :web_url
          ))
          project.to_hash
        end

        desc "Remove project"
        delete do
          Dependabot::Projects::Remover.call(project)
        end
      end

      desc "List all registered projects"
      get do
        Project.all.map(&:to_hash)
      end

      # TODO: Create separate endpoint for syncing existing project in v2
      desc "Add new project or update existing one and sync jobs if it already exists"
      params do
        requires :project, type: String, desc: "Project full path"
        optional :gitlab_access_token, type: String, desc: "Project specific access token"
      end
      post do
        project_name = params[:project]

        log(:info, "Registering project '#{project_name}'")
        project = Dependabot::Projects::Creator.call(project_name, access_token: params[:gitlab_access_token])
        error!("Project is in archived state!", 401) unless project

        Cron::JobSync.call(project)
        project.to_hash
      end
    end

    namespace "/projects/registration" do
      desc "Create project based on system webhook event"
      params do
        requires :event_name, type: String, desc: "Event name"
        optional :path_with_namespace, type: String, desc: "Project full path"
        optional :old_path_with_namespace, type: String, desc: "Project full path before renaming"
      end
      post do
        unless %w[project_create project_destroy project_rename project_transfer].include?(params[:event_name])
          status 202
          return { message: "Skipped, event not supported" }
        end

        unless allowed_namespace?
          status 202
          return { message: "Skipped, does not match allowed namespace pattern" }
        end

        {
          message: Webhooks::SystemHookHandler.call(
            event_name: params[:event_name],
            project_name: params[:path_with_namespace],
            old_project_name: params[:old_path_with_namespace]
          )
        }
      end
    end
  end
end
