# frozen_string_literal: true

module V2
  class UpdateJobs < Grape::API # rubocop:disable Metrics/ClassLength
    include AuthenticationSetup
    include Pagination

    helpers do
      params :update_job_params do
        optional :package_ecosystem, type: String, desc: "Filter by package ecosystem"
      end

      def job_filter_args
        { package_ecosystem: params[:package_ecosystem] }.compact
      end
    end

    namespace :update_jobs do
      helpers do
        def update_job
          @update_job ||= Update::Job.find(params[:id])
        end
      end

      desc "Get all update jobs" do
        detail "Return array of all update jobs"
        is_array true
        success model: Update::Job::Entity,
                examples: [Update::Job::Entity.example_response],
                message: "Successful response"
      end
      params do
        use :pagination_params
        use :update_job_params
      end
      get do
        present paginate(Update::Job.where(**job_filter_args))
      end

      route_param :id, type: String, desc: "Update job id" do
        desc "Get single update job" do
          detail "Return single update job"
          success model: Update::Job::Entity,
                  examples: Update::Job::Entity.example_response,
                  message: "Successful response"
        end
        get do
          present update_job
        end

        desc "Trigger update job" do
          detail "Trigger dependency update job"
          success message: "Successful response", examples: [{ message: "Triggered dependency update job" }]
        end
        post do
          project = Project.find(update_job.project_id)

          UpdateRunnerJob.perform_later(
            project_name: project.name,
            package_ecosystem: update_job.package_ecosystem,
            directory: update_job.directory
          )

          present({ message: "Triggered dependency update job" })
        end

        namespace :runs do
          desc "Get all update job runs" do
            detail "Return array of all update job runs"
            is_array true
            success model: Update::Run::Entity,
                    examples: [Update::Run::Entity.example_response],
                    message: "Successful response"
          end
          params do
            use :pagination_params
            optional :has_failures, type: Boolean, desc: "Filter by runs with failures"
          end
          get do
            runs = update_job.runs
            present paginate(
              params[:has_failures].nil? ? runs.all : runs.where(failed: params[:has_failures])
            )
          end

          route_param :run_id, type: String, desc: "Update run id" do
            helpers do
              def update_run
                update_job.runs.find(params[:run_id])
              end
            end

            desc "Get single update job run" do
              detail "Return single update job run"
              success model: Update::Run::Entity,
                      examples: Update::Run::Entity.example_response,
                      message: "Successful response"
            end
            get do
              present update_run
            end

            desc "Get update job run failures" do
              detail "Return array of update job run failures"
              is_array true
              success model: Update::Failure::Entity,
                      examples: [Update::Failure::Entity.example_response],
                      message: "Successful response"
            end
            params do
              use :pagination_params
            end
            get :failures do
              present paginate(update_run.failures.all)
            end

            desc "Get update job run log entries" do
              detail "Get all update job run log entries"
              is_array true
              success model: Update::LogEntry::Entity,
                      examples: [Update::LogEntry::Entity.example_response],
                      message: "Successful response"
            end
            params do
              use :pagination_params
              optional :log_level,
                       type: String,
                       default: "INFO",
                       values: LogLevel.levels.keys,
                       desc: <<~DESC
                         Return log entries with given log level or above, one of `#{LogLevel.levels.keys}`, default: `INFO`
                       DESC
            end
            get :log_entries do
              present paginate(
                update_run.log_entries.where(:level.gte => LogLevel.to_numeric(params[:log_level]))
              )
            end
          end
        end
      end
    end

    namespace :projects do
      helpers APIHelpers

      route_param :id, type: String, desc: "Project id or url encoded full path" do
        desc "Get all update jobs" do
          detail "Return array of update jobs for a project"
          is_array true
          success model: Update::Job::Entity,
                  examples: [Update::Job::Entity.example_response],
                  message: "Successful response"
        end
        params do
          use :pagination_params
          use :update_job_params
        end
        get :update_jobs do
          update_jobs = project.update_jobs
          present paginate(update_jobs.where(**job_filter_args))
        end
      end
    end
  end
end
