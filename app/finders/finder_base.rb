# frozen_string_literal: true

class FinderBase
  include ApplicationHelper

  def self.find(...)
    new(...).find
  end
end
