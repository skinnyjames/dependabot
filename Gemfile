# frozen_string_literal: true

source "https://rubygems.org"

ruby "~> 3.1"

gem "anyway_config", "~> 2.6"
gem "bcrypt", "~> 3.1"
gem "bootsnap", ">= 1.4.2", require: false
gem "dartsass-sprockets", "~> 3.1"
gem "dependabot-omnibus", "0.239.0"
gem "dry-validation", "~> 1.10"
gem "faraday-retry", "~> 2.2"
gem "gitlab", "~> 4.19", github: "andrcuns/gitlab", branch: "merge-trains-endpoint"
gem "grape", "~> 2.0"
gem "grape-entity", "~> 1.0"
gem "grape-kaminari", "~> 0.4.5"
gem "grape-swagger", "~> 2.0"
gem "graphql-client", "~> 0.18.0"
gem "importmap-rails", "~> 2.0"
gem "json-schema", "~> 4.1"
gem "kaminari-actionview", "~> 1.2"
gem "kaminari-mongoid", "~> 1.0"
gem "kubeclient", "~> 4.11"
gem "lograge", "~> 0.14.0"
gem "mongoid", "~> 8.1"
gem "mongoid_rails_migrations", "~> 1.4"
gem "puma", "~> 6.4"
gem "rails", "~> 7.1.2"
gem "rails-healthcheck", "~> 1.4"
gem "rainbow", "~> 3.1"
gem "redis", "~> 5.0"
gem "request_store", "~> 1.5"
gem "request_store-sidekiq", "~> 0.1.0"
gem "sentry-rails", "~> 5.16", require: false
gem "sentry-sidekiq", "~> 5.16", require: false
gem "sidekiq", "~> 7.2.0"
gem "sidekiq_alive", "~> 2.3.1", require: false, github: "arturictus/sidekiq_alive", branch: "quiet-mode"
gem "sidekiq-cron", "~> 1.12"
gem "sprockets-rails", "~> 3.4"
gem "stackprof", "~> 0.2.25", require: false
gem "stimulus-rails", "~> 1.3"
gem "terminal-table", "~> 3.0"
gem "turbo-rails", "~> 1.5"
gem "tzinfo-data", "~> 1.2023"
gem "warning", "~> 1.3", require: false
gem "yabeda-prometheus", "~> 0.9.1", require: false
gem "yabeda-sidekiq", "~> 0.10.0", require: false

group :test do
  gem "debug", "~> 1.9"
  gem "factory_bot_rails", "~> 6.4"
  gem "faker", "~> 3.2"
  gem "httparty", "~> 0.21.0"
  gem "mustache", "~> 1.1", require: false
  gem "pry-byebug", "~> 3.10"
  gem "reek", "~> 6.2", require: false
  gem "rspec", "~> 3.12"
  gem "rspec_junit_formatter", "~> 0.6.0"
  gem "rspec-rails", "~> 6.1.0"
  gem "rspec-sidekiq", "~> 4.1", require: false
  gem "rubocop", "~> 1.59.0", require: false
  gem "rubocop-performance", "~> 1.20.2", require: false
  gem "rubocop-rails", "~> 2.23", require: false
  gem "rubocop-rspec", "~> 2.26", require: false
  gem "simplecov", "~> 0.22.0", require: false
  gem "simplecov-cobertura", "~> 2.1.0", require: false
  gem "simplecov-console", "~> 0.9.1", require: false
end

group :development do
  gem "git", "~> 1.19", require: false
  gem "grape-swagger-entity", "~> 0.5.2"
  gem "grape-swagger-representable", "~> 0.2.2"
  gem "pry-rails", "~> 0.3.9"
  gem "semver2", "~> 3.4", require: false
  gem "solargraph", "~> 0.50.0", require: false
  gem "solargraph-rails", "~> 1.1", require: false
  gem "spring", "~> 4.1.3", require: false
  gem "spring-commands-rspec", "~> 1.0.4", require: false
end

group :assets do
  gem "bootstrap", "~> 5.3"
end
